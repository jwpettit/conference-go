from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(
        url,
        params=payload,
        headers=headers,
    )
    clue = json.loads(response.content)
    try:
        return {"picture_url": clue["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    # headers = {"Authorization": OPEN_WEATHER_API_KEY}
    payload ={
        "q": f"{city},{state}",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(
        url,
        params=payload,
    )
    clue = json.loads(response.content)
    try:
        lat = clue[0]["lat"]
        lon = clue[0]["lon"]
    except:
        lat = None
        lon = None

    payload2 = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url2 = "https://api.openweathermap.org/data/2.5/weather"
    response2 = requests.get(url, params=payload2)
    content2 = json.loads(response2.content)
    try:
        k_temp = content2["main"]["temp"]
        f_temp = round(1.8 * (k_temp - 273) + 32, 2)
        return {
            "temp": f_temp,
            "description": content2["weather"][0]["description"],
        }
    except:
        return {
            "temp": None,
            "description": None,
        }
